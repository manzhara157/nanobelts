import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.io.FileNotFoundException;
// import java.lang.classfile.constantpool.DoubleEntry;

public class start {

	static double step = 1;
	static double j = 0.000;
	// TODO
	// для перевірки поміняти крок по часу. 0.5, 0.2, 1
	static double dt = 1;
	static String method_of = "no"; // 0 - Ейлера, 1 - Рунге-Кутта
	static char variand = 'b';
	static String file_name = "j" + j + variand + "dt" + dt + ".csv";
	static String file_name_massive = "massive_j" + j + variand + ".csv";

	private static String time_parser(int time_left, int n){
		String line = "";
		String line_h = "";
		// time_left = time_left * (int)(Math.pow(0.5, (2*time_left - 3))+1);
		if (time_left > 60) {
			int t_min = time_left / 60;
			int t_sec = time_left - (t_min * 60);
			if (t_min > 60) {
				int t_hour = t_min / 60;
				t_min = t_min - (t_hour * 60);
				line_h = t_hour + " годин ";
			} else {
				line_h = "";
			}
			line = line_h + t_min + " хвилин " + t_sec + " секунд";
		} else {
			line = "" + time_left;
		}
		return line;
		// return time_left + " sec";
	}


	static double fa(double a, double b, double c, double dt, int ra_kinet, int rb_therm, int rc_therm, double d, double ra_bal) {
		return ra_kinet*( d-(rc_therm/c)-(rb_therm/b)-(ra_bal/ra_kinet)*j);
	}

	static double fb(double a, double b, double c, double dt, int rb_kinet, int ra_therm, int rc_therm, double d, double rb_bal) {
		return rb_kinet*( d-(ra_therm/a)-(rc_therm/c)-(rb_bal/rb_kinet)*j);
	}

	static double fc(double a, double b, double c, double dt, int rc_kinet, int ra_therm, int rb_therm, double d, double rc_bal) {
		return rc_kinet*( d-(ra_therm/a)-(rb_therm/b)-(rc_bal/rc_kinet)*j); 
	}
	
	private static void stop() {

			Scanner myObj = new Scanner(System.in);  // Create a Scanner object
			String userName = myObj.nextLine();  // Read user input

	}

	private static int get_new_lich() {
		step += 0.125;
		return (int)Math.pow(2,step);
	}

	private static double round_to_2(double number) {
		return Math.round(number*10000000000.0)/10000000000.0;
	}

	private static void create_file() {

		try {
			File myObj = new File(file_name);
			if (myObj.exists()) {
				myObj.delete();
			} 

			if (myObj.createNewFile()) {
				System.out.println("File created: " + myObj.getName());
			} else {
				System.out.println("File already exist.");
			}

		} catch (IOException e) {
			System.out.println("An error occured.");
			e.printStackTrace();
		}

		try {
			FileWriter myWriter = new FileWriter(file_name);
      		myWriter.write("ln(t)\tln(a)\tln(b)\tln(c)\tln(d)\tln(v)\tln(sumv)\tln(p)\tln(n)\n");
      		myWriter.close();
      		System.out.println("Successfully wrote to the file.");
		} catch (IOException e) {
    	  System.out.println("An error occurred.");
    	  e.printStackTrace();
    	}
	}

	private static void write_massive(double a, double b, double c) {
		String line1 =  a + "," + b + "," + c + "\n";
		try {
			BufferedWriter myWriter = new BufferedWriter(new FileWriter(file_name_massive, true));
			myWriter.write(line1);
			myWriter.close();
		} catch (IOException e) {
			System.out.println("An error occured");
			e.printStackTrace();
		}
	}

	private static String formated_number(double num) {
		String form_num = String.format("%.17e", num);
		return form_num;
	}

	private static void write_to(double t, double a, double b, double c, double d, double v, double sumv, double p, double n) {
		t = Math.log(t);
		a = Math.log(a);
		b = Math.log(b);
		c = Math.log(c);
		d = Math.log(d);
		v = Math.log(v);
		sumv = Math.log(sumv);
		p = Math.log(p);
		n = Math.log(n);
		String line = formated_number(t) + "\t" + formated_number(a) + "\t" + formated_number(b) + "\t" + formated_number(c) + "\t" + formated_number(d) + "\t" + formated_number(v) + "\t" + formated_number(sumv) + "\t" + formated_number(p) + "\t" + formated_number(n) + "\n";
		try {
			BufferedWriter myWriter = new BufferedWriter(new FileWriter(file_name, true));
			myWriter.write(line);
			myWriter.close();
		} catch (IOException e) {
			System.out.println("An error occured");
			e.printStackTrace();
		}
	}

    public static void main(String[] args) {

		java.awt.Toolkit.getDefaultToolkit().beep();
		if (args.length == 0) {
			System.out.println("No argument.");
			System.out.println("Use: java start [flag] [<arg>]");
			System.out.println("Use argument:");
			System.out.println("flag\t comments");
			System.out.println("-m \t method of calculation:");
			System.out.println("\t\te - Euler method");
			System.out.println("\t\trk - Runge–Kutta methods");
			System.out.println("-dt \t delta time of calculetion");
			System.exit(0);
		} else {
			method_of = args[0];
			for (int i = 0; i < args.length; i++) {
            switch (args[i]) {
                case "-m":
					method_of = args[i+1];
                    break;
                case "-dt":
					String dt_line = args[i+1].replace(',', '.');
					dt = Double.parseDouble(dt_line);
					file_name = "j" + j + variand + "dt" + dt + ".csv";
                    break;
                case "-h":
                    break;
                default:
                    // System.out.println("Невідома опція: " + args);
                    break;
            }
			}
		}
        System.out.println("Start!");
		create_file();

		int n0 = 200000;
		int tmax = 1000000;

   		int ra_kinet = 1;
   		int rb_kinet = 1;
   		int rc_kinet = 1;

   		int ra_therm = 1;
   		int rb_therm = 1;
   		int rc_therm = 1;

   		double xeq = 0.01;
   		double d0 = 0.01;

   		double a0 = 200.0;
   		double b0 = 200.0;
   		double c0 = 200.0;
   		double vtot = n0 * a0 * b0 * c0 * 100;

   		int lich0 = (int)(tmax/dt/50000);
   		// int lich0 = 1;

		double t = 0.1;
		double d = d0;

		double ra_bal = 1;
   		double rb_bal = 1;
   		double rc_bal = 1;
		if (variand == 'a') {
			ra_bal = 1;
   			rb_bal = 1;
   			rc_bal = 1;
		} else {
			ra_bal = 1/2;
   			rb_bal = Math.sqrt(2);
   			rc_bal = Math.sqrt(2);
		}


   		// create_files;

   		double sumv=0;
   		double sump=0;

		double[] a = new double [n0+1];
		double[] b = new double [n0+1];
		double[] c = new double [n0+1];
		double[] anew = new double [n0+1];
		double[] bnew = new double [n0+1];
		double[] cnew = new double [n0+1];

		// String fmean_v, mean_abc, fd, fn, fp, fv;

		double p, v0, suma, sumb, sumc, mean_a, mean_b, mean_c, mean_v;

		try {
			File file_mass = new File(file_name_massive);
			int index = 1;
			if (file_mass.exists()) {
				try {
        		    Scanner scanner = new Scanner(file_mass);
					if (scanner.hasNextLine()) {
					    scanner.nextLine();
            		}

        		    while (scanner.hasNextLine()) {
						String line = scanner.nextLine();
        		        String[] numbers = line.split(",");

        		        if (numbers.length == 3) {
        		            a[index] = Double.parseDouble(numbers[0]);
        		            b[index] = Double.parseDouble(numbers[1]);
        		            c[index] = Double.parseDouble(numbers[2]);
							sumv=sumv+a[index]*b[index]*c[index];
        		            index++;
        		        }
        		    }

        		    scanner.close();
        		    System.out.println("Файл прочитано");

        		} catch (FileNotFoundException e) {
        		    System.out.println("Файл не знайдений");
        		}
			} else {
				if (file_mass.createNewFile()) {
					System.out.println("File created: " + file_mass.getName());
					String line1 = t + "," + d  + "," + n0 + "\n";
					try {
						BufferedWriter myWriter = new BufferedWriter(new FileWriter(file_name_massive, true));
						myWriter.write(line1);
						myWriter.close();
					} catch (IOException e) {
						System.out.println("An error occured");
						e.printStackTrace();
					}
				} else {
					System.out.println("File already exist.");
				}
				for (int i=1; i<n0;i++) {
   				    a[i]=a0*Math.log(1/Math.random());
   				    b[i]=b0*Math.log(1/Math.random());
   				    c[i]=c0*Math.log(1/Math.random());
   				    sumv=sumv+a[i]*b[i]*c[i];
   				    sump=sump+((a[i]*b[i])+(b[i]*c[i])+(c[i]*a[i]));
					write_massive(a[i], b[i], c[i]);
				}
			} 


		} catch (IOException e) {
			System.out.println("An error occured.");
			e.printStackTrace();
		}

   		v0=sumv;
   		int n=n0;

   		int lich=lich0;
   		double tdt_old=lich0;
		long time_start = System.currentTimeMillis();
		long time_old = time_start;
		double iter_per_second = 1; // / (System.currentTimeMillis() - time_start);
		double time_left = 1;

   		while (t<tmax) { 

			t=t+dt;
   		    suma=0;
   		    sumb=0;
   		    sumc=0;
   		    sumv=0;
   		    sump=0;

			int old_n = n;
   		    int i = 1;
   		    while (i<=n-1) {

				// TODO
				// Спробувати метод Рунге-Кутта
				switch (method_of) {
					case "e":
						anew[i]=a[i]+dt*ra_kinet*( d-(rc_therm/c[i])-(rb_therm/b[i])-(ra_bal/ra_kinet)*j );
						bnew[i]=b[i]+dt*rb_kinet*( d-(ra_therm/a[i])-(rc_therm/c[i])-(rb_bal/rb_kinet)*j );
   		      			cnew[i]=c[i]+dt*rc_kinet*( d-(ra_therm/a[i])-(rb_therm/b[i])-(rc_bal/rc_kinet)*j );
						break;
					case "rk":
						double k1a = fa(a[i], b[i], c[i], dt, ra_kinet, rb_therm, rc_therm, d, ra_bal);
						double k1b = fb(a[i], b[i], c[i], dt, rb_kinet, ra_therm, rc_therm, d, rb_bal);
						double k1c = fc(a[i], b[i], c[i], dt, rc_kinet, ra_therm, rb_therm, d, rc_bal);

						double k2a = fa(a[i]+0.5*dt*k1a, b[i]+0.5*dt*k1b, c[i]+0.5*dt*k1c, dt, ra_kinet, rb_therm, rc_therm, d, ra_bal);
						double k2b = fb(a[i]+0.5*dt*k1a, b[i]+0.5*dt*k1b, c[i]+0.5*dt*k1c, dt, rb_kinet, ra_therm, rc_therm, d, rb_bal);
						double k2c = fc(a[i]+0.5*dt*k1a, b[i]+0.5*dt*k1b, c[i]+0.5*dt*k1c, dt, rc_kinet, ra_therm, rb_therm, d, rc_bal);

						double k3a = fa(a[i]+0.5*dt*k2a, b[i]+0.5*dt*k2b, c[i]+0.5*dt*k2c, dt, ra_kinet, rb_therm, rc_therm, d, ra_bal);
						double k3b = fb(a[i]+0.5*dt*k2a, b[i]+0.5*dt*k2b, c[i]+0.5*dt*k2c, dt, rb_kinet, ra_therm, rc_therm, d, rb_bal);
						double k3c = fc(a[i]+0.5*dt*k2a, b[i]+0.5*dt*k2b, c[i]+0.5*dt*k2c, dt, rc_kinet, ra_therm, rb_therm, d, rc_bal);

						double k4a = fa(a[i]+dt*k3a, b[i]+dt*k3b, c[i]+dt*k3c, dt, ra_kinet, rb_therm, rc_therm, d, ra_bal);
						double k4b = fb(a[i]+dt*k3a, b[i]+dt*k3b, c[i]+dt*k3c, dt, rb_kinet, ra_therm, rc_therm, d, rb_bal);
						double k4c = fc(a[i]+dt*k3a, b[i]+dt*k3b, c[i]+dt*k3c, dt, rc_kinet, ra_therm, rb_therm, d, rc_bal);

						anew[i] = a[i] + 0.1666667d * dt * (k1a+2*k2a+2*k3a+k4a);
						bnew[i] = b[i] + 0.1666667d * dt * (k1b+2*k2b+2*k3b+k4b);
						cnew[i] = c[i] + 0.1666667d * dt * (k1c+2*k2c+2*k3c+k4c);

						// if (n == old_n) {
							// System.out.println("\rk1a:\t" + k1a + " | k1b:\t" + k1b + " | k1c:\t" + k1c);
							// System.out.println("\rk2a:\t" + k2a + " | k2b:\t" + k2b + " | k2c:\t" + k2c);
							// System.out.println("\rk3a:\t" + k3a + " | k3b:\t" + k3b + " | k3c:\t" + k3c);
							// System.out.println("\rk4a:\t" + k4a + " | k4b:\t" + k4b + " | k4c:\t" + k4c);
							// System.out.println("\ra[" + i + "]\t" + a[i] + " b[" + i + "]\t" + b[i] + " c[" + i + "]\t" + c[i]);
							// System.out.println("\ranew[" + i + "]\t" + anew[i] + " bnew[" + i + "]" + bnew[i] + " cnew[" + i + "]\t" + cnew[i]);
							// System.out.println(d);
							// System.out.println(n);
							// stop();
						// }
						break;
					default: 
						System.out.println("No argument.");
						System.out.println("Use: java start [<arg>]");
						System.out.println("Use argument:");
						System.out.println("e \tEuler method");
						System.out.println("rk \tRunge–Kutta methods");
						System.exit(0);
				}


   		      	if (anew[i]<0 || bnew[i]<0 || cnew[i]<0) {

					// System.out.println("a[" + i + "]" + anew[i] + "b[" + i + "]" + bnew[i] + "c[" + i + "]" + cnew[i]);
					a[i]= a[n];
   		      	 	b[i]= b[n];
   		      	 	c[i]= c[n];
   		      	 	n=n-1;
					// stop();

			  	} else {

			  	  suma=suma+anew[i];
   		      	  sumb=sumb+bnew[i];
   		      	  sumc=sumc+cnew[i];
   		      	  sumv=sumv+anew[i]*bnew[i]*cnew[i];
   		      	  sump=sump+((anew[i]*bnew[i])+(bnew[i]*cnew[i])+(cnew[i]*anew[i]));
   		      	  i=i+1;

			  	}
			}

			a=anew;
   		 	b=bnew;
   		 	c=cnew;

   		 	d=((xeq+d0)*(1-v0/vtot)+v0/vtot-sumv/vtot)/(1-sumv/vtot)-xeq;
   		 	p=2*sump;

			long period = System.currentTimeMillis() - time_old;
			if (period > 1000) {

				period = period / 1000; // тепер це в секундах
			   	iter_per_second = (t/dt - tdt_old) / period;
			   	time_left = (time_left + ((period/(t/dt - tdt_old))*(tmax-t)))/2; 
				int multiplayer = (int)(40.69*Math.pow(time_left,-0.4227)+1); // Модифікатор часу для точнішої передачі

			   	System.out.print("\rПрогрес: " + (Math.round(t)*100/tmax) + "/100% | n: " + n + " | it/sec: " + (int)iter_per_second + " | залишилось часу: " + time_parser((int)(time_left*multiplayer), n) + " секунд");
			   	time_old = System.currentTimeMillis();
				tdt_old = t/dt;

			}

			// Запис у документ з певною періодичністю
			if (lich == lich0) {

				mean_a=suma/n;
   			   	mean_b=sumb/n;
   			   	mean_c=sumc/n;
   			   	mean_v=sumv/n;
			   	
							// System.out.println("\nxeq " + xeq);
							// System.out.println("\nd0 " + d0);
							// System.out.println("\nv0 " + v0);
							// System.out.println("\nvtot " + vtot);
							// System.out.println("\nsumv " + sumv);
							// stop();

				write_to(t, mean_a, mean_b, mean_c, d, mean_v, sumv, p, n);

				lich=0;
				// lich0 = 500; // get_new_lich();
			}

			lich=lich+1;
		}
		long duration = (System.currentTimeMillis() - time_start) / 1000;
		if (duration < 60) {
			System.out.println("");
			System.out.println("Кількість n: " + n);
			System.out.println("Done! Total time " + duration + " seconds.");
		} else {
			long d_min = duration / 60;
			long d_sec = duration - (d_min * 60);
			System.out.println("");
			System.out.println("Кількість n: " + n);
			System.out.println("Done! Total time is " + d_min + " minute " + d_sec + " seconds.");
		} 
		java.awt.Toolkit.getDefaultToolkit().beep();
	}
}
