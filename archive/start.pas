Program StartPas;

uses
  Math;


const
   n0 = 20000;
   {n0 = 200000;}

   {z:=12;}

   tmax = 100000;
   {tmax = 1000000;}
   dt = 0.1;

   ra_kinet = 1;
   rb_kinet = 1;
   rc_kinet = 1;

   ra_therm = 1;
   rb_therm = 1;
   rc_therm = 1;


   {ra_bal = 1;
   rb_bal = 1;
   rc_bal = 1;        }


   j = 0.008;
   xeq = 0.01;
   d0 = 0.01;

   a0 = 200.0;
   b0 = 200.0;
   c0 = 200.0;
   vtot = n0 * a0 * b0 * c0 * 100;

   lich0 = 100;


var

   a,b,c,anew,bnew,cnew:array[1..n0] of real;

   fmean_v, mean_abc, fd, fn, fp, fv, a_mass, b_mass, c_mass: text;

   p, t, d, v0, sump, suma, sumb, sumc, sumv, mean_a, mean_b, mean_c, mean_v: real;
   
   n, i, lich:Longint;

  ra_bal,rb_bal,rc_bal:extended;



procedure create_files;
begin
  Assign(mean_abc,'mean_abc.csv');
  Rewrite(mean_abc);
  Close(mean_abc);

  Assign(fmean_v,'fmean_v.csv');
  Rewrite(fmean_v);
  Close(fmean_v);

  Assign(fd,'fd.csv');
  Rewrite(fd);
  Close(fd);

  Assign(fn,'fn.csv');
  Rewrite(fn);
  Close(fn);

  Assign(fp,'fp.csv');
  Rewrite(fp);
  Close(fp);

  Assign(fv,'fv.csv');
  Rewrite(fv);
  Close(fv);

  Assign(a_mass,'a.csv');
  Rewrite(a_mass);
  Close(a_mass);

  Assign(b_mass,'b.csv');
  Rewrite(b_mass);
  Close(b_mass);

  Assign(c_mass,'c.csv');
  Rewrite(c_mass);
  Close(c_mass);
end;


procedure save_data;
begin
  Append(mean_abc);
  Writeln(mean_abc, ln(t),' ',ln(mean_a),' ',ln(mean_b),' ',ln(mean_c));
  Close(mean_abc);

  Append(fmean_v);
  Writeln(fmean_v, ln(t),' ',ln(mean_v));
  Close(fmean_v);

  Append(fd);
  Writeln(fd, ln(t),' ',ln(d));
  Close(fd);

  Append(fn);
  Writeln(fn, t,' ',n);
  Close(fn);

  Append(fp);
  Writeln(fp, ln(t),' ',ln(p));
  Close(fp);

  Append(fv);
  Writeln(fv, ln(t),' ',sumv);
  Close(fv);

end;

{procedure save_abc;
begin

  Append(a_mass);
  Writeln(a_mass, a);
  Close(a_mass);

  Append(b_mass);
  Writeln(b_mass, b);
  Close(b_mass);

  Append(c_mass);
  Writeln(c_mass, c);
  Close(c_mass);

end;}

begin

   t:=0;
   d:=d0;

   ra_bal:= 1/2;
   rb_bal:= Sqrt(2);
   rc_bal:= Sqrt(2);


   create_files;

   sumv:=0;
   sump:=0;
	   {for i:=1 to n0 do}
   i := 1;
   while i<=n0 do
   begin
       	a[i]:=a0*0.9; {ln(1/random);}
       	b[i]:=b0*0.9; {ln(1/random);}
       	c[i]:=c0*0.9; {ln(1/random);}
       	sumv:=sumv+a[i]*b[i]*c[i];
       	sump:=sump+((a[i]*b[i])+(b[i]*c[i])+(c[i]*a[i]));

		Append(a_mass);
  		Writeln(a_mass, a[i]);
  		Close(a_mass);

  		Append(b_mass);
  		Writeln(b_mass, b[i]);
  		Close(b_mass);

  		Append(c_mass);
  		Writeln(c_mass, c[i]);
  		Close(c_mass);

		i:=i+1;
   end;

   v0:=sumv;
   n:=n0;

   lich:=lich0;

   while t<tmax do
   begin

       t:=t+dt;
       suma:=0;
       sumb:=0;
       sumc:=0;
       sumv:=0;
       sump:=0;

       i := 1;
       while i<=n do
       begin

         anew[i]:=a[i]+dt*ra_kinet*( d-(rc_therm/c[i])-(rb_therm/b[i])-(ra_bal/ra_kinet)*j );
         bnew[i]:=b[i]+dt*rb_kinet*( d-(ra_therm/a[i])-(rc_therm/c[i])-(rb_bal/rb_kinet)*j );
         cnew[i]:=c[i]+dt*rc_kinet*( d-(ra_therm/a[i])-(rb_therm/b[i])-(rc_bal/rc_kinet)*j );

         if (anew[i]<0) or (bnew[i]<0) or (cnew[i]<0) then
         begin

          a[i]:= a[n];
          b[i]:= b[n];
          c[i]:= c[n];
          n:=n-1;
         end
	   else
       begin

        suma:= suma+anew[i];
        sumb:= sumb+bnew[i];
        sumc:= sumc+cnew[i];
        sumv:= sumv+anew[i]*bnew[i]*cnew[i];
        sump:=sump+((anew[i]*bnew[i])+(bnew[i]*cnew[i])+(cnew[i]*anew[i]));
        i:=i+1;

	   end;

    end;

    a:=anew;
    b:=bnew;
    c:=cnew;

    d:=((xeq+d0)*(1-v0/vtot)+v0/vtot-sumv/vtot)/(1-sumv/vtot)-xeq;
    p:=2*sump;

    if lich = lich0 then
    begin

		mean_a:=suma/n;
      	mean_b:=sumb/n;
      	mean_c:=sumc/n;
		mean_v:=sumv/n;
		writeln('n: ', n, ' | sumv: ', sumv, ' | progress: ', round(t), '/', tmax, ' iter.');
		save_data;

        lich:=0;
      end;

	  lich:=lich+1;

end;

end.
