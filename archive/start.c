#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <gmp.h>
/* #include <mpzr.h> */

/* Constants */
const int n0 = 20000; // тестові значення
const int tmax = 10000; // тестові значення
/* const int n0 = 200000; */
/* const int tmax = 1000000; */
const float dt = 0.1;

int ra_kinet = 1;
int rb_kinet = 1;
int rc_kinet = 1;

int ra_therm = 1;
int rb_therm = 1;
int rc_therm = 1;

const float j = 0.008;
const float xeq = 0.01;
const float d0 = 0.01;

float a0 = 200.0;
float b0 = 200.0;
float c0 = 200.0;
double vtot = 100.0;

clock_t time_req;
clock_t total_time;

float rand01() {
	float num = (rand() % (1000000) + 1) / 1000000.0;
	return num;
}

int create_file() {
	char fname[] = "data/abc_avg.csv";
	FILE *fp = fopen(fname, "w");
	if (fp == NULL) {
		printf("Error opening the file %s", fname);
		return -1;
	}
    char text[] = "t,a avg,b avg,c avg\n";
	fprintf(fp, text);
	fclose(fp);

	strcpy(fname, "data/v_avg.csv");
	fp = fopen(fname, "w");
	if (fp == NULL) {
		printf("Error opening the file %s", fname);
		return -1;
	}
    strcpy(text, "ln(t),ln(v)\n");
	fprintf(fp, text);
	fclose(fp);

	strcpy(fname, "data/d_avg.csv");
	fp = fopen(fname, "w");
	if (fp == NULL) {
		printf("Error opening the file %s", fname);
		return -1;
	}
    strcpy(text, "ln(t),ln(d)\n");
	fprintf(fp, text);
	fclose(fp);

	strcpy(fname, "data/n_avg.csv");
	fp = fopen(fname, "w");
	if (fp == NULL) {
		printf("Error opening the file %s", fname);
		return -1;
	}
    strcpy(text, "t,n\n");
	fprintf(fp, text);
	fclose(fp);

	strcpy(fname, "data/p_avg.csv");
	fp = fopen(fname, "w");
	if (fp == NULL) {
		printf("Error opening the file %s", fname);
		return -1;
	}
    strcpy(text, "ln(t),ln(p)\n");
	fprintf(fp, text);
	fclose(fp);

	strcpy(fname, "data/sumv.csv");
	fp = fopen(fname, "w");
	if (fp == NULL) {
		printf("Error opening the file %s", fname);
		return -1;
	}
    strcpy(text, "ln(t),ln(sumv)\n");
	fprintf(fp, text);
	fclose(fp);
	return 0;
}


int write_to_file(float time, float v, float d, float n, float p, double sumv) {
	char fname_v_avg[] = "data/v_avg.csv";
	char fname_d_avg[] = "data/d_avg.csv";
	char fname_n_avg[] = "data/n_avg.csv";
	char fname_p_avg[] = "data/p_avg.csv";
	char fname_sumv[]  = "data/sumv.csv";
	float log_time = log(time);
	float log_v = log(v);
	float log_d = log(d);
	float log_n = log(n);
	float log_p = log(p);
	float log_sumv = sumv; //log(sumv);

	FILE *fp_v = fopen(fname_v_avg, "a");
	if (fp_v == NULL) {
		printf("Error opening the file %s", fname_v_avg);
	} else {
		fprintf(fp_v, "%f,%f\n", log_time, log_v);
	}
	fclose(fp_v);

	FILE *fp_d = fopen(fname_d_avg, "a");
	if (fp_d == NULL) {
		printf("Error opening the file %s", fname_d_avg);
	} else {
		fprintf(fp_d, "%f,%f\n", log_time, log_d);
	}
	fclose(fp_d);

	FILE *fp_n = fopen(fname_n_avg, "a");
	if (fp_n == NULL) {
		printf("Error opening the file %s", fname_n_avg);
	} else {
		fprintf(fp_n, "%f,%f\n", time, n);
	}
	fclose(fp_n);

	FILE *fp_p = fopen(fname_p_avg, "a");
	if (fp_p == NULL) {
		printf("Error opening the file %s", fname_p_avg);
	} else {
		fprintf(fp_p, "%f,%f\n", log_time, log_p);
	}
	fclose(fp_p);

	FILE *fp_sumv = fopen(fname_sumv, "a");
	if (fp_sumv == NULL) {
		printf("Error opening the file %s", fname_sumv);
	} else {
		fprintf(fp_sumv, "%f,%.3f\n", log_time, log_sumv);
	}
	fclose(fp_sumv);

	return 0;
}

int write_to_file_abc(float time, float a, float b, float c) {
	char fname[] = "data/abc_avg.csv";
	FILE *fp = fopen(fname, "a");
	if (fp == NULL) {
		printf("Error opening the file %s", fname);
		return -1;
	}
	fprintf(fp, "%f,%f,%f,%f\n", time, a, b, c);
	fclose(fp);
	return 0;
}

/* int read_mass(int abc) { */
/* 	if (abc == 1) { */
/* 		char fname[] = "data/a.csv"; */
/* 	} */
/* 	if (abc == 2) { */
/* 		char fname[] = "data/b.csv"; */
/* 	} */
/* 	if (abc == 3) { */
/* 		char fname[] = "data/c.csv"; */
/* 	} */
/* 	FILE *fp = fopen(fname, "r"); */
/* 	if (fp == NULL) { */
/* 		printf("Error opening the file %s", fname); */
/* 		return -1; */
/* 	} */
/* 	char line[100]; */
/* 	while(fgets(line, 100, fp)) { */
		
/* 	} */
/* 	fclose(fp); */
/* 	return 0; */
/* } */

int create_to_file_abc(int i, float a, float b, float c) {
	char fname[] = "data/abc_mass.csv";
	FILE *fp = fopen(fname, "a");
	if (fp == NULL) {
		printf("Error opening the file %s", fname);
		return -1;
	}
	fprintf(fp, "%d,%f,%f,%f\n", i, a, b, c);
	fclose(fp);
	return 0;
}

int main(int argc, char *argv[])
{
	vtot = n0 * a0 * b0 * c0 * 100;
	/* printf("n0: %d | a0: %f | b0: %f | c0: %lf | vtot: %e \n", n0, a0, b0, c0, vtot); */
	srand(time(NULL));
    printf("Start!");
	time_t seconds_start = time(NULL);
	time_t seconds = time(NULL);
	/* seconds_old = time(NULL); */
	/* time_req = clock(); */
	/* total_time = clock(); */

	/* Variable */
	float a[n0];
	float b[n0];
	float c[n0];
	float a_new[n0];
	float b_new[n0];
	float c_new[n0];
	/* mean_a і інші це середне значення */
	float p, suma, sumb, sumc, mean_a, mean_b, mean_c, mean_v = 0.0;


	float ra_bal = 1.0/2.0;
	float rb_bal = sqrt(2);
	float rc_bal = sqrt(2);
	float d = d0;

    /* Перезапис старих файлів, якщо вони є. Обов'язково скопіюйте результати після виконання програми. */
	printf("Cтворення нових файлів\n");
    create_file();

	mpf_t sumv;
	mpf_init_set_d(sumv, 0.0);
	int sump = 0;
//	int i = 0;
    for (int i = 0; i <= n0; i++) {
		a[i] = a0 * 0.9; //log(1.0/rand01());
		b[i] = b0 * 0.9; //log(1.0/rand01());
		c[i] = c0 * 0.9; //log(1.0/rand01());
		/* printf("num: %f\n", a[i]); */
		double sumv_cur = a[i] * b[i] * c[i]; 
		mpf_add_ui(sumv, sumv, sumv_cur);
		create_to_file_abc(i, a[i], b[i], c[i]);
		/* printf("i: %d | a: %.3f | b: %.3f | c: %.3f | sumv: %.3f \n", i, a[i], b[i], c[i], sumv_cur); */
	}

    int n = n0;
	/* printf("n is : %d \n", n); */
	double v0 = mpf_get_d(sumv);

	float t = 1;
	float t_old = 0;
	int it_per_sec = 0;
	float time_left = 1;
	char is_write = 1;
	int t_is_write = 0.0;
	int lich = 100;
	
	while (t < tmax) { // має бути t < tmax
		/* printf("n in : %d \n", n); */
		t = t + dt;
		if (n < 1) break;
		suma = 0.0;
		sumb = 0.0;
		sumc = 0.0;
		mpf_set_d(sumv, 0.0);
		sump = 0.0; // Воно нікуди не записалось і обнулилось

		/* int i = 1; */
		for (int i = 0; i <= n; i++) {
			a_new[i] = a[i] + dt * ra_kinet * (d - (rc_therm / c[i]) - (rb_therm / b[i]) - (ra_bal / ra_kinet) * j);
			b_new[i] = b[i] + dt * rb_kinet * (d - (ra_therm / a[i]) - (rc_therm / c[i]) - (rb_bal / rb_kinet) * j);
			c_new[i] = c[i] + dt * rc_kinet * (d - (ra_therm / a[i]) - (rb_therm / b[i]) - (rc_bal / rc_kinet) * j);
			/* if (a_new[i] < 0) { */
			/* 	a_new[i] = d0; */
			/* } */

			/* if (b_new[i] < 0) { */
			/* 	b_new[i] = d0; */
			/* } */

			/* if (c_new[i] < 0) { */
			/* 	c_new[i] = d0; */
			/* } */
			/* if (a_new[i] <= d0 && b_new[i] <= d0 && c_new[i] <= d0) { */
			if ((a_new[i] < 0) || (b_new[i] < 0) || (c_new[i] < 0)) {
				a[i] = a[n];
				b[i] = b[n];
				c[i] = c[n];
                n = n - 1;
			}
			else {
				suma = suma + a_new[i];
				sumb = sumb + b_new[i];
				sumc = sumc + c_new[i];
				double sumv_cur = a_new[i] * b_new[i] * c_new[i];
				mpf_add_ui(sumv, sumv, sumv_cur);
				sump = sump + ((a_new[i] * b_new[i]) + (b_new[i] * c_new[i]) + (c_new[i] * a_new[i]));
			}
			/* i += 1; */
		}

		for (int i = 0; i < n; i++) {
			a[i] = a_new[i];
			b[i] = b_new[i];
			c[i] = c_new[i];
		}

		double sumv_double = mpf_get_d(sumv);
//		d:= ((xeq + d0) * (1   - v0/vtot) + v0/vtot - sumv		 /vtot) / (1   - sumv		/vtot) - xeq; Це перевірочна формула з Pascal
		d = ((xeq + d0) * (1.0 - v0/vtot) + v0/vtot - sumv_double/vtot) / (1.0 - sumv_double/vtot) - xeq;
		p = 2 * sump;

		/* printf("d: %f, xeq: %f | v0: %d | vtot: %d | sumv: %d \r", d, xeq, v0, vtot, sumv_double ); */
		
		int t_int = (int)t;
		/* if (t_int != t_is_write) is_write = 1; */

		/* if (t_int%1 == 0 && is_write == 1) { */
		if (lich == 100) {
			/* is_write = 0; */
			/* t_is_write = t_int; */
			mean_a = suma / n;
			mean_b = sumb / n;
			mean_c = sumc / n;
			mean_v = sumv_double / n;
            /* write_to_file_abc(t, mean_a, mean_b, mean_c); */
            /* write_to_file(t, mean_v, d, n ,p, sumv_double); */
			/* printf("t: %f | n: %d \r", t, n); */
			int period = time(NULL) - seconds;
			time_left = ((time(NULL) - seconds_start)/t)*(tmax/t)*100000;
			/* if (period > 2) { */
				seconds = time(NULL);
				it_per_sec = (t - t_old) / period;
				int perc = t_int * 100 / tmax;
				if (time_left < 60) {
					printf("%d/100 | it/sec %d | sumv: %f | ln(d): %f | n: %d | Time left: %f seconds     \r", perc, it_per_sec, sumv_double, log(d), n, time_left );
				} else {
					if (time_left > 3600) {
						time_left = time_left / 3600;
						printf("%d/100 | it/sec %d | sumv: %f | n: %d | Time left: %f hours    \r", perc, it_per_sec, sumv_double, n, time_left );
					} else {
						time_left = time_left / 60;
						printf("%d/100 | it/sec %d | sumv: %f | n: %d | Time left: %f minutes    \r", perc, it_per_sec, sumv_double, n, time_left );
					}
				}


				/* if (time_left > 86400) { */
				/* 	time_left = time_left / 86400; */
				/* 	printf("%d/100 | it/sec %d | d: %f | n: %d | Time left: %f days\r", perc, it_per_sec, d, n, time_left ); */
				/* } */
				/* printf("%d/100 | it/sec %d | d: %f | n: %d \r", perc, it_per_sec, d, n ); */
				t_old = t;
				fflush(stdout);
			/* } */
			lich = 0;
		}
		lich++;
	}
	time_t seconds_new = time(NULL);
	printf("\nTotal time %d sec.", seconds_new - seconds_start);
	
    return 0;
}
