unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Math, StdCtrls, TeEngine, Series, ExtCtrls, TeeProcs, Chart;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Chart1: TChart;
    Series1: TPointSeries;
    Series2: TPointSeries;
    Series3: TPointSeries;
    Chart2: TChart;
    Series4: TPointSeries;
    Button2: TButton;
    Chart3: TChart;
    Chart4: TChart;
    Series5: TPointSeries;
    Series6: TPointSeries;
    Chart5: TChart;
    Series7: TPointSeries;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;


const
   n0 = 200000;

   z:=12;

   tmax = 1000000;
   dt = 0.1;

   ra_kinet = 1;
   rb_kinet = 1;
   rc_kinet = 1;

   ra_therm = 1;
   rb_therm = 1;
   rc_therm = 1;


   {ra_bal = 1;
   rb_bal = 1;
   rc_bal = 1;        }


   j = 0.008;
   xeq = 0.01;
   d0 = 0.01;

   a0 = 200.0;
   b0 = 200.0;
   c0 = 200.0;
   vtot = n0 * a0 * b0 * c0 * 100;

   lich0 = 100;


var
   Form1: TForm1;
   n: integer;

   a,b,c,state,anew,bnew,cnew:array[1..n0] of real;

   fmean_v, mean_abc, fd, fn, fp, fv:textfile;

   p, t, d, v0, sump, suma, sumb, sumc, sumv, mean_a, mean_b, mean_c, mean_v: real;





implementation

{$R *.dfm}


procedure create_files;
begin
  AssignFile(mean_abc,'mean_abc.txt');
  Rewrite(mean_abc);
  CloseFile(mean_abc);

  AssignFile(fmean_v,'fmean_v.txt');
  Rewrite(fmean_v);
  CloseFile(fmean_v);

  AssignFile(fd,'fd.txt');
  Rewrite(fd);
  CloseFile(fd);

  AssignFile(fn,'fn.txt');
  Rewrite(fn);
  CloseFile(fn);

  AssignFile(fp,'fp.txt');
  Rewrite(fp);
  CloseFile(fp);

  AssignFile(fv,'fv.txt');
  Rewrite(fv);
  CloseFile(fv);
end;


procedure save_data;
begin
  Append(mean_abc);
  Writeln(mean_abc, ln(t),' ',ln(mean_a),' ',ln(mean_b),' ',ln(mean_c));
  CloseFile(mean_abc);

  Append(fmean_v);
  Writeln(fmean_v, ln(t),' ',ln(mean_v));
  CloseFile(fmean_v);

  Append(fd);
  Writeln(fd, ln(t),' ',ln(d));
  CloseFile(fd);

  Append(fn);
  Writeln(fn, ln(t),' ',ln(n));
  CloseFile(fn);

  Append(fp);
  Writeln(fp, ln(t),' ',ln(p));
  CloseFile(fp);

  Append(fv);
  Writeln(fv, ln(t),' ',ln(sumv));
  CloseFile(fv);
end;


procedure TForm1.Button1Click(Sender: TObject);
var
  i, j, lich:integer;

  ra_bal,rb_bal,rc_bal:extended;




begin

   t:=0;
   d:=d0;

   ra_bal:= 1/2;
   rb_bal:= Sqrt(2);
   rc_bal:= Sqrt(2);


   create_files;

   sumv:=0;
   sump:=0;
   for i:=1 to n0 do
   begin
       state[i]:=1;
       a[i]:=a0*ln(1/random);
       b[i]:=b0*ln(1/random);
       c[i]:=c0*ln(1/random);
       sumv:=sumv+a[i]*b[i]*c[i];
       sump:=sump+((a[i]*b[i])+(b[i]*c[i])+(c[i]*a[i]));
   end;

   v0:=sumv;
   n:=n0;

   lich:=lich0;

   while t<tmax do
   begin

       t:=t+dt;
       suma:=0;
       sumb:=0;
       sumc:=0;
       sumv:=0;
       sump:=0;

       i := 1;
       while i<=n do
       begin

         anew[i]:=a[i]+dt*ra_kinet*( d-(rc_therm/c[i])-(rb_therm/b[i])-(ra_bal/ra_kinet)*j );
         bnew[i]:=b[i]+dt*rb_kinet*( d-(ra_therm/a[i])-(rc_therm/c[i])-(rb_bal/rb_kinet)*j );
         cnew[i]:=c[i]+dt*rc_kinet*( d-(ra_therm/a[i])-(rb_therm/b[i])-(rc_bal/rc_kinet)*j );

         if (anew[i]<0) or (bnew[i]<0) or (cnew[i]<0) then
         begin

          a[i]:= a[n];
          b[i]:= b[n];
          c[i]:= c[n];
          n:=n-1;
         end
      else
      begin

        suma:= suma+anew[i];
        sumb:= sumb+bnew[i];
        sumc:= sumc+cnew[i];
        sumv:= sumv+anew[i]*bnew[i]*cnew[i];
        sump:=sump+((anew[i]*bnew[i])+(bnew[i]*cnew[i])+(cnew[i]*anew[i]));
        i:=i+1;

      end;

    end;

    a:=anew;
    b:=bnew;
    c:=cnew;

    d:=((xeq+d0)*(1-v0/vtot)+v0/vtot-sumv/vtot)/(1-sumv/vtot)-xeq;
    p:=2*sump;

    if lich = lich0 then
    begin

      mean_a:=suma/n;
      mean_b:=sumb/n;
      mean_c:=sumc/n;
      mean_v:=sumv/n;

       { Series1.AddXY(ln(t), ln(mean_a));
        Series2.AddXY(ln(t), ln(mean_b));
        Series3.AddXY(ln(t), ln(mean_c));
        Series4.AddXY(ln(t), ln(mean_v));
        Series5.AddXY(ln(t), ln(d));
        Series6.AddXY(ln(t), ln(n));
        Series7.AddXY(ln(t), ln(p));  }
        save_data;

        lich:=0;
      end;

      lich:=lich+1;

        {

  if n <= n0/2 then

  begin

  for i=1 to n
  for j=1 to n
  if j<>i then
  s[j]:= sqrt (sqrt(a[j]-a[i])+sqrt(b[j]-b[i])+sqrt(c[j]-c[i]));


  end;



  sa:=0;
  sb:=0;
  sc:=0;


  for k=1 to z do

  sa:=a[j[k]]-a[i]+sa;
  sb:=b[j[k]]-b[i]+sb;
  sc:=c[j[k]]-c[i]+sc;

  anew[i]:=a[i]+sa/z;
  bnew[i]:=b[i]+sb/z;
  cnew[i]:=c[i]+sc/z;


  }



      application.ProcessMessages;

   end;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  halt
end;

end.







