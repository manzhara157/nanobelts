# using ProgressMeter
using ProgressBars
using LinearAlgebra
using Distributed
using Printf
# export OPENBLAS_NUM_THREADS=4
# Constants
#
# Пробні константи
const n0 = 20000
const tmax = 100000
# Основні Константи
# const n0 = 200000
# const tmax = 1000000
const dt = 0.1

ra_kinet = 1
rb_kinet = 1
rc_kinet = 1

ra_therm = 1
rb_therm = 1
rc_therm = 1

const j = 0.008
const xeq = 0.01
const d0 = 0.01

a0 = b0 = c0 = 200
vtot = n0 * a0 * b0 * c0 * 100

# lich0 = 100
# addprocs(1)
# w = workers()
println("Кількість процесів: ", nprocs())

# function progress_bar()
# 	iter = ProgressBar(1:10000)
# 	Threads.@threads for i in iter
# 		loss = exp(-i)
#         set_multiline_postfix(iter, "Test 1: $(rand())\nTest 2: $(rand())\nTest 3: $loss)")
# 	end
# end
	
function create_file()

# 	fname = "data/all.csv"
# 	f = open(fname, "w")
#     text = "time, mean_a, mean_b, mean_c, mean_v, d, n, p, sumv"
# 	println(f, text)
	# close(f)

	fname = "data/abc_mass.csv"
	f = open(fname, "w")
    text = "i,a,b,c"
	println(f, text)
	close(f)


	fname = "data/v_avg.csv"
	f = open(fname, "w")
    text = "t,v"
	println(f, text)
	close(f)

	fname = "data/d_avg.csv"
	f = open(fname, "w")
    text = "t,d"
	println(f, text)
	close(f)

	fname = "data/n_avg.csv"
	f = open(fname, "w")
    text = "t,n"
	println(f, text)
	close(f)

	fname = "data/p_avg.csv"
	f = open(fname, "w")
    text = "t,p"
	println(f, text)
	close(f)

	fname = "data/v_sum.csv"
	f = open(fname, "w")
    text = "t,sumv"
	println(f, text)
	close(f)
end

# function write_all_to_file(time, mean_a, mean_b, mean_c, mean_v, d, n, p, sumv )
# 	fname = "data/all.csv"
# 	f = open(fname, "a")
# 	text = string(time, ",", mean_a, ",", mean_b, ",", mean_c, ",", mean_v, ",", d, ",", n, ",", p, ",", sumv)
# 	println(f, text)
# 	close(f)
# end

function write_to_file(file_name, time, data)
	fname = "data/" * file_name * ".csv"
	f = open(fname, "a")
	text = string(time, ",", Complex(data))
	println(f, text)
	close(f)
end

function write_abc(file_name, data)
	fname = "data/" * file_name * ".csv"
	f = open(fname, "a")
	text = string(data)
	println(f, text)
	close(f)
end

function write_to_file_abc(time, a, b, c)
	fname = "data/abc_avg.csv"
	f = open(fname, "a")
	text = string(time, ",", a, ",", b, ",", c)
	println(f, text)
	close(f)
end

function create_to_file_abc(i, a, b, c)
	fname = "data/abc_mass.csv"
	f = open(fname, "a")
	text = string(i, ",", a, ",", b, ",", c)
	println(f, text)
	close(f)
end

function calc_a_new(a, dt, ra_kinet, d, rc_therm, c, rb_therm, b, ra_bal, j)
	return a + dt * ra_kinet * (d - (rc_therm / c) - (rb_therm / b) - (ra_bal / ra_kinet) * j)
end

function calc_b_new(b, dt, rb_kinet, d, ra_therm, a, rc_therm, c, rb_bal, j)
	return b + dt * rb_kinet * (d - (ra_therm / a) - (rc_therm / c) - (rb_bal / rb_kinet) * j)
end

function start_calculating()

	println("Start!")
	print("Кількість процесів: ")
	println(nprocs())
	
	# Variable
	a = Array{Float64, 1}(undef, n0)
	b = c = a_new = b_new = c_new = Array{Float64, 1}(undef, n0)
    # state =
	# mean_a і інші це середне значення
	p = suma = sumb = sumc = mean_a = mean_b = mean_c = mean_v = 0.0 
	
	# Begin
	
	ra_bal = 1
	rb_bal = 1
	rc_bal = 1
	d = d0
	
    # Перезапис старих файлів, якщо вони є. Обов'язково скопіюйте результати після виконання програми.
	println("Cтворення нових файлів")
    create_file()
	
	sumv = 0
	sump = 0
	
	prg = ProgressBar(1:n0)
	for i in prg
		# state[i] = 1
		a[i] = a0 * 0.9 #log(1/rand())
		b[i] = b0 * 0.9 #log(1/rand())
		c[i] = c0 * 0.9 #log(1/rand())
		sumv = sumv + a[i] + b[i] + c[i]
		sump = sump + ((a[i] * b[i]) + (b[i] * c[i]) + (c[i] * a[i]))
		# create_to_file_abc(i,a[i],b[i],c[i])
		# write_abc("a", a[i])
		# write_abc("b", b[i])
		# write_abc("yc", c[i])
		set_description(prg, string(@sprintf("Створення початкового масиву: %.2f", i)))
	end
	
	write_abc("a", a)
	exit()
	# n = n0
    n = n0
	v0 = sumv
	
	# lich = lich0
	
	BLAS.set_num_threads(Sys.CPU_THREADS)
	t = 0
	t_progress_bar = [ 0:dt:tmax; ]
	# tpb = ProgressBar(t_progress_bar)
	tpb = [ 0:dt:tmax; ]
	timeOld = time()
	time_left = 1
	mesure = " seconds"
	# @showprogress desc="Обрахунок зміни нанопоясків" for t in t_progress_bar # Це реалізація циклу через ProgressMeter
	for t in tpb
		# t = round(t + dt; digits = 2)
		# t > tmax && break
		# print("t: ")
		println(t)
		suma = 0
		sumb = 0
		sumc = 0
		sumv = 0
		sump = 0 # Воно нікуди не записалось і обнулилось
	
		i = 1
		while true
            i > n && break
			a_new[i] = a[i] + dt * ra_kinet * (d - (rc_therm / c[i]) - (rb_therm / b[i]) - (ra_bal / ra_kinet) * j)
			b_new[i] = b[i] + dt * rb_kinet * (d - (ra_therm / a[i]) - (rc_therm / c[i]) - (rb_bal / rb_kinet) * j)
			c_new[i] = c[i] + dt * rc_kinet * (d - (ra_therm / a[i]) - (rb_therm / b[i]) - (rc_bal / rc_kinet) * j)
	
			if (a_new[i] < 0) || (b_new[i] < 0) || (c_new[i] < 0)
				a[i] = a[n]
				b[i] = b[n]
				c[i] = c[n]
                # deleteat!(a, i)
                # deleteat!(b, i)
                # deleteat!(c, i)
                n -= 1
			else
				# suma = suma + a_new[i] // замінено на вбудовану функцію sum()
				# sumb = sumb + b_new[i]
				# sumc = sumc + c_new[i]
				sumv = sumv + a_new[i] * b_new[i] * c_new[i]
				sump = sump + ((a_new[i] * b_new[i]) + (b_new[i] * c_new[i]) + (c_new[i] * a_new[i]))
			end
			i += 1
		end
		a = a_new
		b = b_new
		c = c_new
	
		d = ((xeq+d0) * (1 - v0/vtot) + v0/vtot - sumv/vtot) / (1 - sumv/vtot) - xeq
		p = 2 * sump
		# println("vtot: ", vtot)
	
		if t%10 == 0
			mean_a = sum(a) / n
			mean_b = sum(b) / n
			mean_c = sum(c) / n
			mean_v = sumv / n
			# write_all_to_file(time, mean_a, mean_b, mean_c, mean_v, d, n, p, sumv)
            # write_to_file_abc(t, mean_a, mean_b, mean_c)
            # write_to_file("v_avg", t, mean_v)
            # write_to_file("d_avg", t, d)
            # write_to_file("n_avg", t, n)
            # write_to_file("p_avg", t, p)
            # write_to_file("v_sum", t, sumv)
			time_left = (time()-timeOld/t)*(tmax-t)/1000
			mesure = " seconds"
			if time_left > 60
				time_left = time_left / 60
				mesure = " minutes"
			end
			if time_left > 3600
				time_left = time_left / 3600
				mesure = " hours"
			end
			if time_left > 86400
				time_left = time_left / 86400
				mesure = " days"
			end
			timeOld = time()
			print(round(t/10000; digits = 3),"%/",tmax/10000,"% | Time: ", round(time_left; digits = 3), mesure, " left | it/sec ", round(t/(time()-timeOld); digits = 3), " | n: ", n, " \r")
			flush(stdout)
		end
		# set_description(tpb, string(@sprintf("Ідуть обрахунки. n=%.2f.", n)))
	end

end


# progress_bar()
start_calculating()
